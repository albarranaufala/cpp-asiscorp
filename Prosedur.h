#ifndef PROSEDUR_H_INCLUDED
#define PROSEDUR_H_INCLUDED
#include <iostream>
#include <fstream>
#include <string>
#include <conio.h>
#include <stdlib.h>
#include <sstream>
#include <time.h>

using namespace std;
string username, username1, username2, username3,username4, password, password1, customer, hargamasuk, barang, duitkeluar;
string loadcustomer="", loadharga="", loadtanggal="", loadbarang="", loadtanggalout="", loadhargaout="";
char c1,c2,c3,c4,c5,c6,c7,c8;
string customerIN[1000], barangOUT[1000];
string tanggalIN[1000],tanggalOUT[1000];
int hargaIN[1000],hargaOUT[1000];
float totalIN,totalOUT;
float persentasegajikaryawan,gajipersonal;
void headermenu();
void menulanjutkaryawan();
void menulanjutmanager();
void atasan();
void pilihankaryawan();
void pilihanmanager();
void tambahpemasukan();
void pilihanmanagerataukaryawan();
void tambahpengeluaran();


void Register(){
    ofstream simpanakun;
    cout << "Username baru: "; cin >> username;
    cout << "Password baru: "; cin >> password;
    cout << "Password lagi: "; cin >> password1;
    username = username + ".txt";
    if(password==password1){
        cout << "Register berhasil" << endl;
        simpanakun.open(username.c_str());
        simpanakun << password;
        simpanakun.close();
    }
    else{
        cout << "Password tidak sama" << endl;
    }
    cout << endl <<"Press any key to continue..." << endl;
    c2 = getch();
    headermenu();
}

void Login(){
    ifstream cariakun;
    cout << "Username: "; cin >> username;
    cout << "Password: "; cin >> password;
    username1 = username + ".txt";
    cariakun.open(username1.c_str());
    if(cariakun.is_open()){
        int z = 0;
        string fileisi[1000];
        while(cariakun){
            cariakun >> fileisi[z];
            z++;
        }
        password1 = fileisi[0];
        if(password1==password){
            cout << "Login berhasil" << endl;
            cout << endl <<"Press any key to continue..." << endl;
            c2 = getch();
            pilihanmanagerataukaryawan();
        }
        else{
            cout << "Username atau password salah" << endl;
            cout << endl <<"Press any key to continue..."<< endl;
            c2 = getch();
            headermenu();
        }
    }
    else{
        cout << "Username atau password salah" << endl;
        cout << endl <<"Press any key to continue..."<< endl;
        c2 = getch();
        headermenu();
    }
    cariakun.close();
}

void pilihanmanagerataukaryawan(){
    system("cls");
    atasan();
    cout << "||       Masuk sebagai:      ||" << endl;
    cout << "||       [1] Karyawan        ||" << endl;
    cout << "||       [2] Manager         ||" << endl;
    cout << "===============================" << endl;
    c5 = getch();
    if(c5=='1'){
        menulanjutkaryawan();
    }
    else if(c5=='2'){
        string kodeperusahaan;
        ifstream isikode;
        isikode.open("kode_perusahaan.txt");
        isikode >> kodeperusahaan;
        isikode.close();
        string kodeIN;
        cout << "Masukkan kode perusahaan: ";cin >> kodeIN;
        if(kodeIN==kodeperusahaan){
            cout << "Kode benar" << endl;
            cout << endl <<"Press any key to continue..."<< endl;
            c2 = getch();
            menulanjutmanager();
        }
        else{
            cout << "Kode salah" << endl;
            cout << endl <<"Press any key to continue..."<< endl;
            c2 = getch();
            pilihanmanagerataukaryawan();
        }
    }
    else{
        pilihanmanagerataukaryawan();
    }
}

void pengeluarankaryawan(){
    loadbarang="";
    loadtanggalout="";
    loadhargaout="";
    totalOUT=0;
    string pengeluaran[1000];
    ifstream barangout;
    ifstream tanggalout;
    ifstream hargabarangout;
    barangout.open("out_barang.txt");
    tanggalout.open("out_tanggal.txt");
    hargabarangout.open("out_harga.txt");
    int u=-1;
    while(!barangout.eof()){
        u=u+1;
        getline(barangout, barangOUT[u]);
        loadbarang = loadbarang + barangOUT[u] + "\n";
        pengeluaran[u] = barangOUT[u] + " // ";
    }
    barangout.close();
    u=-1;
    while(!tanggalout.eof()){
        u=u+1;
        getline(tanggalout, tanggalOUT[u]);
        loadtanggalout = loadtanggalout + tanggalOUT[u] + "\n";
    }
    pengeluaran[0] = pengeluaran[0] + tanggalOUT[0] + " // ";
    int j = 1;
    int k = 0;
    while(j<=u){
        k=k+1;
        pengeluaran[k] = pengeluaran[k] + tanggalOUT[j] + " // ";
        j=j+2;
    }
    tanggalout.close();
    u=-1;
    while(!hargabarangout.eof()){
        u=u+1;
        hargabarangout >> hargaOUT[u];
        totalOUT = totalOUT + hargaOUT[u];
        string hargaOUTstr;
        stringstream jj;
        jj << hargaOUT[u];
        hargaOUTstr = jj.str();
        loadhargaout = loadhargaout + hargaOUTstr + "\n";
        pengeluaran[u]=pengeluaran[u]+hargaOUTstr;
    }
    hargabarangout.close();
    cout << endl;
    for(int j=0;j<=u;j++){
        if(j==0){
            cout << "-------------------------------" << endl;
        }
        else{
            cout << j <<". "<< pengeluaran[j] << endl;
        }
    }
    cout << endl << "Total pengeluaran: " << totalOUT << endl;
    cout << "Tambah pengeluaran? " << endl;
    cout << "[1] Ya" << endl;
    cout << "[2] Tidak" << endl;
    c4 = getch();
    if(c4=='1')
        tambahpengeluaran();
    else{
        if(c5=='1'){
            menulanjutkaryawan();
        }
        else if(c5=='2'){
            menulanjutmanager();
        }
    }
}

void pemasukankaryawan(){
    loadcustomer="";
    loadharga="";
    loadtanggal="";
    totalIN=0;
    string pemasukan[1000];
    ifstream customerin;
    ifstream tanggalin;
    ifstream hargain;
    customerin.open("in_customer.txt");
    tanggalin.open("in_tanggal.txt");
    hargain.open("in_harga.txt");
    int a=-1;
    while(!customerin.eof()){
        a=a+1;
        getline(customerin, customerIN[a]);
        loadcustomer = loadcustomer + customerIN[a] + "\n";
        pemasukan[a] = customerIN[a] + " // ";
    }
    customerin.close();
    a=-1;
    while(!tanggalin.eof()){
        a=a+1;
        getline(tanggalin, tanggalIN[a]);
        loadtanggal = loadtanggal + tanggalIN[a] + "\n";
    }
    pemasukan[0] = pemasukan [0] + tanggalIN[0] + " // ";
    int b = 1;
    int c = 0;
    while(b<=a){
        c=c+1;
        pemasukan[c] = pemasukan[c] + tanggalIN[b] + " // ";
        b=b+2;
    }
    a=-1;
    while(!hargain.eof()){
        a=a+1;
        hargain >> hargaIN[a];
        totalIN = totalIN + hargaIN[a];
        string hargaINstr;
        stringstream ss;
        ss << hargaIN[a];
        hargaINstr = ss.str();
        loadharga = loadharga + hargaINstr + "\n";
        pemasukan[a] = pemasukan[a] + hargaINstr;
    }
    hargain.close();
    cout << endl;
    for(int b=0;b<=a;b++){
        if(b==0){
            cout << "-------------------------------" << endl;
        }
        else{
            cout << b <<". "<< pemasukan[b] << endl;
        }
    }
    cout << endl << "Total pemasukan: " << totalIN << endl;
    cout << "Tambah pemasukan? " << endl;
    cout << "[1] Ya" << endl;
    cout << "[2] Tidak" << endl;
    c4 = getch();
    if(c4=='1')
        tambahpemasukan();
    else{
        if(c5=='1'){
            menulanjutkaryawan();
        }
        else if(c5=='2'){
            menulanjutmanager();
        }
    }
}

void tambahpemasukan(){
    ofstream Customer;
    ofstream tanggalMasuk;
    ofstream hargaMasuk;
    string tanggalup;
    ofstream gajisave;
    float gajisebagian;
    time_t waktu;
    time(&waktu);
    tanggalup = ctime(&waktu);
    cout << endl;
    cout << "Nama customer: "; cin >> customer;
    cout << "Tanggal      : " << tanggalup;
    cout << "Harga        : "; cin >> hargamasuk;
    Customer.open("in_customer.txt");
    tanggalMasuk.open("in_tanggal.txt");
    hargaMasuk.open("in_harga.txt");
    int hargamasukint = atoi(hargamasuk.c_str());
    gajisebagian = (hargamasukint*persentasegajikaryawan);
    gajipersonal = gajipersonal + gajisebagian;
    gajisave.open(username1.c_str());
    string gajipersonalstr;
    stringstream sementara;
    sementara << gajipersonal;
    gajipersonalstr = sementara.str();
    string masukfile = password1 + "\n" + gajipersonalstr;
    gajisave << masukfile;
    gajisave.close();
    loadcustomer = loadcustomer + customer;
    loadtanggal = loadtanggal + tanggalup;
    loadharga = loadharga + hargamasuk;
    Customer << loadcustomer;
    tanggalMasuk << loadtanggal;
    hargaMasuk << loadharga;
    Customer.close();
    tanggalMasuk.close();
    hargaMasuk.close();
    cout << "Menambah pemasukan berhasil" << endl;
    cout << endl <<"Press any key to continue..."<< endl;
    c2 = getch();
    if(c5=='1'){
        menulanjutkaryawan();
    }
    else if(c5=='2'){
        menulanjutmanager();
    }
}

void tambahpengeluaran(){
    ofstream Barangout;
    ofstream tanggalKeluar;
    ofstream hargaKeluar;
    string tanggalup1;
    time_t waktumasuk;
    time(&waktumasuk);
    tanggalup1=ctime(&waktumasuk);
    cout << endl;
    cout << "Nama barang: "; cin >> barang;
    cout << "Tanggal    : " << tanggalup1;
    cout << "Harga      : "; cin >> duitkeluar;
    Barangout.open("out_barang.txt");
    tanggalKeluar.open("out_tanggal.txt");
    hargaKeluar.open("out_harga.txt");
    loadbarang = loadbarang + barang;
    loadtanggalout = loadtanggalout + tanggalup1;
    loadhargaout = loadhargaout + duitkeluar;
    Barangout << loadbarang;
    tanggalKeluar << loadtanggalout;
    hargaKeluar << loadhargaout;
    Barangout.close();
    tanggalKeluar.close();
    hargaKeluar.close();
    cout << "Menambah pengeluaran berhasil" << endl;
    cout << endl <<"Press any key to continue..."<< endl;
    c2 = getch();
    if(c5=='1'){
        menulanjutkaryawan();
    }
    else if(c5=='2'){
        menulanjutmanager();
    }
}

void gaji(){
    cout << "Pendapatan anda saat ini: " << gajipersonal << endl;
    cout << endl <<"Press any key to continue..."<< endl;
    c2 = getch();
    menulanjutkaryawan();
}

float keuntunganperusahaan(float jumlahin, float jumlahout, float persenkaryawan){
    float sementara;
    sementara = (jumlahin-(jumlahin*persenkaryawan))-jumlahout;
    return sementara;
}

void analitik(){
    ifstream masukanin;
    ifstream masukanout;
    ifstream persentasekaryawan;
    float persenkaryawan;
    persentasekaryawan.open("persentase_karyawan.txt");
    persentasekaryawan >> persenkaryawan;
    persentasekaryawan.close();
    float jumlahin = 0;
    float isiin=0;
    float isiout=0;
    float jumlahout = 0;
    masukanin.open("in_harga.txt");
    masukanout.open("out_harga.txt");
    while(!masukanin.eof()){
        masukanin >> isiin;
        jumlahin = jumlahin + isiin;
    }
    while(!masukanout.eof()){
        masukanout >> isiout;
        jumlahout = jumlahout + isiout;
    }
    masukanin.close();
    masukanout.close();
    cout << "Analitik keuangan: " << endl;
    cout << "Pemasukan    : " << jumlahin << endl;
    cout << "Pengeluaran  : " << jumlahout << endl;
    cout << "Gaji karyawan: " << jumlahin*persenkaryawan << endl;
    cout << endl;
    if(jumlahin>jumlahout){
        cout << "Perusahaan anda mendapatkan keuntungan sebesar Rp " << (jumlahin-jumlahout) << endl;
        cout << "atau " << ((jumlahin-jumlahout)/jumlahout)*100 << "% dari pengeluaran anda" << endl << endl;
    }
    else if(jumlahin<jumlahout){
        cout << "Perusahaan anda telah rugi sebesar Rp " << (jumlahout-jumlahin) << endl;
        cout << "atau " << ((jumlahin-jumlahout)/jumlahout)*100 << "% dari pengeluaran anda" << endl << endl;
    }
    if(jumlahin-(jumlahin*persenkaryawan)>jumlahout){
        cout << "dan perusahaan anda juga meraup kas perusahaan sebesar Rp " << keuntunganperusahaan(jumlahin, jumlahout, persenkaryawan) << endl;
        cout << "atau " << (keuntunganperusahaan(jumlahin, jumlahout, persenkaryawan)/jumlahout)*100 << "% dari pengeluaran + gaji karyawan anda" << endl;
    }
    else if(jumlahin-(jumlahin*persenkaryawan)<jumlahout){
        cout << "dan keuangan kas perusahaan anda kekurangan sebesar Rp " << jumlahout-(jumlahin-(jumlahin*persenkaryawan)) << endl;
        cout << "atau " << (keuntunganperusahaan(jumlahin, jumlahout, persenkaryawan)/jumlahout)*100 << "% dari pengeluaran + gaji karyawan anda" << endl;
    }
    else if(jumlahin-(jumlahin*persenkaryawan)==jumlahout){
        cout << "dan perusahaan anda tidak mendapatkan kas" << endl;
    }
    cout << endl <<"Press any key to continue..."<< endl;
    c2 = getch();
    menulanjutmanager();
}

void ubahpersen(){
    float persenkar;
    float angkakar;
    cout << "Berapa persen karyawan mendapatkan\npendapatan dari pemasukan? ";
    cin >> persenkar;
    angkakar = persenkar/100;
    ofstream in;
    in.open("persentase_karyawan.txt");
    in << angkakar;
    in.close();
    cout << endl << "Mengganti persentase berhasil" << endl;
    cout << endl <<"Press any key to continue..."<< endl;
    c2 = getch();
    menulanjutmanager();
}

void persentasegaji(){
    ifstream masukanin;
    float persentasekaryawan;
    masukanin.open("persentase_karyawan.txt");
    masukanin >> persentasekaryawan;
    masukanin.close();
    cout << "Persentase keuangan:" << endl;
    cout << "Karyawan  : " << persentasekaryawan*100 << "%" << endl;
    cout << "Perusahaan: " << 100-(persentasekaryawan*100) << "%" << endl<< endl;
    cout << "Mau mengubah?" << endl;
    cout << "[1] Ya" << endl;
    cout << "[2] Tidak" << endl;
    c7 = getch();
    if(c7=='1'){
        ubahpersen();
    }
    else{
        menulanjutmanager();
    }

}

void tentangaplikasi(){
    system("cls");
    string t = "\t\t\t\t";
    cout << t << "          Selamat datang di ASISCORP v1.0" << endl<< endl;
    cout << t << "Fungsi dari aplikasi ini adalah untuk memudahkan"<< endl;
    cout << t << "pengusaha kecil yang kesulitan dalam mengatur:" << endl;
    cout << t << "      1. Pencatatan pemasukan dan pengeluaran"<< endl;
    cout << t << "      2. Pemantauan keuangan perusahaan"<<endl;
    cout << t << "      3. dan sistem bagi hasil antara karyawan" << endl;
    cout << t << "         dan perusahaan" << endl << endl<< endl;
    cout << t << "      Pengembang aplikasi:"<< endl;
    cout << t << "      1. Albarra Naufala Erdanto (18523158)" << endl;
    cout << t << "      2. Ferdian Nursulistio     (18523149)" << endl;
    cout << t << "      3. Rillo Muhammad Surgawa  (18523185)"<< endl;
    cout << t << "      4. Andrew Farros           (18523164)" << endl;
    cout << endl <<t << "      Press any key to continue..."<< endl;
    c2 = getch();
    headermenu();
}

void gantikode(){
    string kodeperusahaan;
    cout << "Ingin mengganti kode perusahaan?" << endl;
    cout << "[1] Ya" << endl;
    cout << "[2] Tidak" << endl;
    c8 = getch();
    if (c8=='1'){
        cout << endl << "Masukan kode baru perusahaan: ";
        cin >> kodeperusahaan;
        ofstream kode;
        kode.open("kode_perusahaan.txt");
        kode << kodeperusahaan;
        kode.close();
        cout << endl << "Mengubah kode perusahaan berhasil"<< endl;
        cout << endl <<"Press any key to continue..."<< endl;
        c2 = getch();
        menulanjutmanager();
    }
    else{
        menulanjutmanager();
    }

}

void menulanjutkaryawan(){
    system ("cls");
    atasan();
    cout << "||      status: karyawan     ||" << endl;
    cout << "||                           ||" << endl;
    cout << "||   [1] Pemasukan           ||" << endl;
    cout << "||   [2] Pengeluaran         ||" << endl;
    cout << "||   [3] Pendapatan          ||" << endl;
    cout << "||   [4] Logout              ||" << endl;
    cout << "===============================" << endl;
    ifstream persentasegaji;
    persentasegaji.open("persentase_karyawan.txt");
    persentasegaji >> persentasegajikaryawan;
    persentasegaji.close();
    ifstream gajiload;
    gajiload.open(username1.c_str());
    int num=0;
    string isifile[1000];
    while(gajiload){
        gajiload >> isifile[num];
        num++;
    }
    float isifilefloat = atoi(isifile[1].c_str());
    gajipersonal = isifilefloat;
    gajiload.close();
    pilihankaryawan();
}

void menulanjutmanager(){
    system ("cls");
    atasan();
    cout << "||      status: manager      ||" << endl;
    cout << "||                           ||" << endl;
    cout << "||   [1] Pemasukan           ||" << endl;
    cout << "||   [2] Pengeluaran         ||" << endl;
    cout << "||   [3] Analitik keuangan   ||" << endl;
    cout << "||   [4] Persentase gaji     ||" << endl;
    cout << "||   [5] Ganti kode          ||" << endl;
    cout << "||   [6] Logout              ||" << endl;
    cout << "===============================" << endl;
    ifstream persentasegaji;
    persentasegaji.open("persentase_karyawan.txt");
    persentasegaji >> persentasegajikaryawan;
    persentasegaji.close();
    ifstream gajiload;
    gajiload.open(username1.c_str());
    int num=0;
    string isifile[1000];
    while(gajiload){
        gajiload >> isifile[num];
        num++;
    }
    float isifilefloat = atoi(isifile[1].c_str());
    gajipersonal = isifilefloat;
    gajiload.close();
    pilihanmanager();
}

void headermenu(){
    system("cls");
    atasan();
    cout << "||      [1] Login            ||" << endl;
    cout << "||      [2] Register         ||" << endl;
    cout << "||      [3] Tentang app      ||" << endl;
    cout << "||      [4] Keluar           ||" << endl;
    cout << "===============================" << endl;
    c1 = getch();
    if(c1=='1')
        Login();
    else if(c1=='2')
        Register();
    else if(c1=='3')
        tentangaplikasi();
    else if(c1=='4')
        exit(0);
    else
        headermenu();
}

void atasan(){
    cout << "===============================" << endl;
    cout << "||       Selamat Datang      ||" << endl;
    cout << "||          Asiscorp         ||" << endl;
    cout << "||            v1.0           ||" << endl;
    cout << "||                           ||" << endl;
}

void pilihankaryawan(){
    c3=getch();
    if(c3=='1'){
        pemasukankaryawan();
    }
    else if(c3=='2'){
        pengeluarankaryawan();
    }
    else if(c3=='3'){
        gaji();
    }
    else if(c3=='4'){
        headermenu();
    }
    else{
        menulanjutkaryawan();
    }
}

void pilihanmanager(){
    c6=getch();
    if(c6=='1'){
        pemasukankaryawan();
    }
    else if(c6=='2'){
        pengeluarankaryawan();
    }
    else if(c6=='3'){
        analitik();
    }
    else if(c6=='4'){
        persentasegaji();
    }
    else if(c6=='5'){
        gantikode();
    }
    else if(c6=='6'){
        headermenu();
    }
    else{
        menulanjutmanager();
    }
}


#endif // PROSEDUR_H_INCLUDED
